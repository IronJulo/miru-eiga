# Miru Eiga

This projet is a movies managment system.     
**Features :**
- Account 
- Add movie 
- Remove movie 
- Edit movie    
- Search a movie    
- Like a movie   

We use **PHP 7.4** to run this website. Please be sure that you use a correct version of PHP.    

You can try our projet on this website (**hosted by a member of the team**).    
http://sabisu.me/.website/miru-eiga/pages/login.php      

You can try with the account or create a new one.     
- **username** : user1    
- **password** : user1user1     


The database is also hosted on this server.      
Check our creation script to deploy your own instance.      
Please **to establish a connection to the database**, create a file named **config.php** in **/include/config** folder. And follow the template in **README** to config auth.    

You also **need** to change in **pages/home.php** at the end of the file the hostname and the path of access.     
