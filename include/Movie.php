<?php

require_once '../include/component/utils.php';

class Movie
{

    /*
    +------------------+--------------+------+-----+---------------------+----------------+
    | Field            | Type         | Null | Key | Default             | Extra          |
    +------------------+--------------+------+-----+---------------------+----------------+
    | idMovies         | int(11)      | NO   | PRI | NULL                | auto_increment |
    | title            | varchar(50)  | NO   | UNI | NULL                |                |
    | url              | text         | NO   |     | NULL                |                |
    | descriptionMovie | text         | NO   |     | NULL                |                |
    | created_at       | datetime     | YES  |     | current_timestamp() |                |
    | producer         | varchar(255) | NO   |     | NULL                |                |
    | gender           | varchar(255) | NO   |     | NULL                |                |
    | idUserAuthor     | int(11)      | NO   |     | NULL                |                |
    +------------------+--------------+------+-----+---------------------+----------------+
    */

    private int $idMovies;
    private string $title;
    private string $url;
    private string $descriptionMovie;
    private string $created_at;
    private string $producer;
    private string $gender;
    private string $releaseDate;
    private int $idUserAuthor;


    public function __construct(int $idMovies, string $title, string $url, string $descriptionMovie, string $created_at, string $producer, string $gender, string $releaseDate, int $idUserAuthor)
    {
        $this->idMovies = $idMovies;
        $this->title = $title;
        $this->url = $url;
        $this->descriptionMovie = $descriptionMovie;
        $this->created_at = $created_at;
        $this->producer = $producer;
        $this->gender = $gender;
        $this->releaseDate = $releaseDate;
        $this->idUserAuthor = $idUserAuthor;


    }

    /**
     * @return int
     */
    public function getIdMovies(): int
    {
        return $this->idMovies;
    }

    /**
     * @param int $idMovies
     */
    public function setIdMovies(int $idMovies): void
    {
        $this->idMovies = $idMovies;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return strip_tags_xss($this->title);
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = strip_tags_xss($title);
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return strip_tags_xss($this->url);
    }

    /**
     * @param string $url
     */
    public function setUrl(string $url): void
    {
        $this->url = strip_tags_xss($url);
    }

    /**
     * @return string
     */
    public function getDescriptionMovie(): string
    {
        return strip_tags_xss($this->descriptionMovie);
    }

    /**
     * @param string $descriptionMovie
     */
    public function setDescriptionMovie(string $descriptionMovie): void
    {
        $this->descriptionMovie = $descriptionMovie;
    }

    /**
     * @return string
     */
    public function getCreatedAt(): string
    {
        return strip_tags_xss($this->created_at);
    }

    /**
     * @param string $created_at
     */
    public function setCreatedAt(string $created_at): void
    {
        $this->created_at = strip_tags_xss($created_at);
    }

    /**
     * @return string
     */
    public function getProducer(): string
    {
        return strip_tags_xss($this->producer);
    }

    /**
     * @param string $producer
     */
    public function setProducer(string $producer): void
    {
        $this->producer = strip_tags_xss($producer);
    }

    /**
     * @return string
     */
    public function getGender(): string
    {
        return strip_tags_xss($this->gender);
    }

    /**
     * @param string $gender
     */
    public function setGender(string $gender): void
    {
        $this->gender = strip_tags_xss($gender);
    }

    /**
     * @return int
     */
    public function getIdUserAuthor(): int
    {
        return $this->idUserAuthor;
    }

    /**
     * @param int $idUserAuthor
     */
    public function setIdUserAuthor(int $idUserAuthor): void
    {
        $this->idUserAuthor = $idUserAuthor;
    }

    /**
     * @return string
     */
    public function getReleaseDate(): string
    {
        return strip_tags_xss($this->releaseDate);
    }

    /**
     * @param string $releaseDate
     */
    public function setReleaseDate(string $releaseDate): void
    {
        $this->releaseDate = strip_tags_xss($releaseDate);
    }



    public function __toString()
    {

        return "[Movie] Id : {$this->getIdMovies()} Title : {$this->getTitle()} URL : {} Description {$this->getDescriptionMovie()} Created at : {$this->getCreatedAt()} Producer : {$this->getProducer()} Gender : {$this->getGender()} Release Date : {$this->getReleaseDate()} User Author Id : {$this->getIdUserAuthor()}";

    }

    public static function drawModalDelete(Movie $movie): void
    {
        echo "
        <div id=\"cancel-modal-{$movie->getIdMovies()}\" class=\"modal\">
            <div class=\"modal-content\">
                <h5><b>Warning</b> ! Do you rely want to remove <b>{$movie->getTitle()}</b> from your movies ?</h5>
                <p>This action will remove the movies from the database !</p>
            </div>
                <div class=\"modal-footer\">
                <a href=\"../pages/removemovie.php?id={$movie->getIdMovies()}\" class=\"modal-close waves-effect waves-green green-text btn-flat\">Agree</a>
                <a href=\"#!\" class=\"modal-close waves-effect waves-red red-text btn-flat\">Cancel</a>
            </div>
        </div>";

    }

    /**
     * Draw home horizontal cards
     */
    public function drawCardItem()
    {

        echo "
            <div class=\"col s12 m7 l12\">
            <h2 class=\"header\">{$this->getTitle()}</h2>
            <div class=\"card horizontal\">
                <div class=\"card-image\" >
                    <img class='materialboxed' src=\"{$this->getUrl()}\">
                </div>
                <div class=\"card-stacked\">
                    <div class=\"card-content\">
                        <p>{$this->getDescriptionMovie()}</p>
                    </div>
                    <div class=\"card-action\">
                
                        <a href=\"#{$this->getIdMovies()}\" class=\"secondary-content modal-trigger\">
                            <i class=\"material-icons medium orange-text\">edit</i>
                        </a>
                        
                        <a href=\"#cancel-modal-{$this->getIdMovies()}\" class=\"secondary-content modal-trigger\">
                            <i class=\"material-icons medium red-text\">delete</i>
                        </a>
                    </div>
                </div>
            </div>
        </div>";

        self::drawModalDelete($this);
    }



    public function drawListItem()
    {

        $this->drawCardItem();

        // TODO ->Implement caracter counter  $('input#input_text, textarea#textarea1').characterCounter();
        echo "
   
          <div id=\"{$this->getIdMovies()}\" class=\"modal modal-fixed-footer\">
            <div class=\"modal-content\">
              <h4>Edit {$this->getTitle()}</h4>
              
                
        
            <div class=\"row\">
                    <form class=\"col s12\" method='GET' action='../pages/updatemovie.php' id='edit-form{$this->getIdMovies()}'>
                    
                        <input id='id{$this->getIdMovies()}' style='visibility: hidden' name=\"id\">
                        
                        <div class=\"row\">
                            <div class=\"input-field col s6\">
                                <input id=\"title{$this->getIdMovies()}\" name=\"title\" maxlength=\"150\" type=\"text\" class=\"validate\">
                                <label for=\"title{$this->getIdMovies()}\">Title</label>
                            </div>

                        </div>
                        <div class=\"row\">
                            <div class=\"input-field col s12\">
                            <textarea id='desc{$this->getIdMovies()}' maxlength=\"1000\" name=\"description\" class=\"materialize-textarea\"></textarea>
                            <label for=\"desc{$this->getIdMovies()}\">Description</label>
                            </div>
                         </div>
                         
                         
                         <div class=\"row\">
                            <div class=\"input-field col s12\">
                            <textarea id='urlPicture{$this->getIdMovies()}' name=\"url\" class=\"materialize-textarea\"></textarea>
                            <label for=\"urlPicture{$this->getIdMovies()}\">URL Movie Picture</label>
                            </div>
                         </div>
                         
                         <div class=\"row\">
                            <div class=\"input-field col s12 l6\">
                                <textarea id='gender{$this->getIdMovies()}' name=\"gender\" maxlength=\"50\" class=\"materialize-textarea\"></textarea>
                                <label for=\"gender{$this->getIdMovies()}\">Gender</label>
                            </div>
                            <div class=\"input-field col s12 l6\">
                                <textarea id='prod{$this->getIdMovies()}' name=\"producer\" maxlength=\"50\" class=\"materialize-textarea\"></textarea>
                                <label for=\"prod{$this->getIdMovies()}\">Producer</label>
                            </div>
                         </div>
                         
                         <div class='row'>
                            <div class=\"input-field col s12\">
                                <textarea class=\"materialize-textarea\" maxlength=\"50\" name=\"release\" id=\"release{$this->getIdMovies()}\"></textarea>
                                <label for=\"release{$this->getIdMovies()}\">Date of release</label>
                            </div>
                        </div>
                         
                        <div class=\"row\">
                        
                            <div class=\"input-field col s12\">
                                <input disabled value=\"{$this->getCreatedAt()}\" id=\"disabled\"  type=\"text\" class=\"validate\">
                                <label for=\"disabled\">Date of creation</label>
                            </div>
                            
                           
                            
                            <div class=\"input-field col s12\">
                                <input disabled value=\"You\" id=\"disabled\" type=\"text\" class=\"validate\">
                                <label for=\"disabled\">Author</label>
                            </div>
                        </div>
                      
                     
                    </form>
                </div>
              <script>
              
                // todo refactor this 
                 var x = document.getElementById(encodeURIComponent(\"desc{$this->getIdMovies()}\")).value;
                 document.getElementById(\"desc{$this->getIdMovies()}\").value = x+\"{$this->getDescriptionMovie()}\";
                
                 var y = document.getElementById(encodeURIComponent(\"title{$this->getIdMovies()}\")).value;
                 document.getElementById(\"title{$this->getIdMovies()}\").value = x+\"{$this->getTitle()}\";
                 
                 var y = document.getElementById(encodeURIComponent(\"gender{$this->getIdMovies()}\")).value;
                 document.getElementById(\"gender{$this->getIdMovies()}\").value = x+\"{$this->getGender()}\";
                 
                 var y = document.getElementById(encodeURIComponent(\"urlPicture{$this->getIdMovies()}\")).value;
                 document.getElementById(\"urlPicture{$this->getIdMovies()}\").value = x+\"{$this->getUrl()}\";
                
                 var y = document.getElementById(encodeURIComponent(\"prod{$this->getIdMovies()}\")).value;
                 document.getElementById(\"prod{$this->getIdMovies()}\").value = x+\"{$this->getProducer()}\";
                 
                 var y = document.getElementById(encodeURIComponent(\"release{$this->getIdMovies()}\")).value;
                 document.getElementById(\"release{$this->getIdMovies()}\").value = x+\"{$this->getReleaseDate()}\";
                 
                 document.getElementById(\"id{$this->getIdMovies()}\").value = {$this->getIdMovies()}
                  
              </script>
              
            </div>
            
            
            <div class=\"modal-footer\">
              <a href=\"#\" class=\"modal-close waves-effect waves-green btn-flat red-text\">Cancel</a>
              <a id=\"edit{$this->getIdMovies()}\" class=\"modal-close waves-effect waves-green btn-flat green-text\">Edit</a>
            </div>
          </div>
            <script>
             $(\"#edit{$this->getIdMovies()}\").click(function() {
                 $(\"#edit-form{$this->getIdMovies()}\").submit();
                });
            </script>" ;



    }
    
    public function drawCard($idUser)
    {
        echo "<div class=\"col s12 m6 \" >
                <div class=\"card blue-grey darken-1 \">
                  <div class=\"card-content white-text cardbody\">
                    <span class=\"card-title center-align\">{$this->getTitle()}</span>
                    <div class='imgcontainer center-align'>
                        <img class='materialboxed' src=\"{$this->getUrl()}\">
                    </div>
                    <p class=\"truncate modal-trigger\" data-target=\"modal" . $this->getIdMovies() . "\">{$this->getDescriptionMovie()}</p>
                  </div>
                </div>
              </div>";
        $this->drawCardModal($idUser);
    }
    
    public function drawCardModal($idUser)
    {
        /**TODO submit btn for like**/
        require_once "component/utils.php";
        echo "
            <div id=\"modal" . $this->getIdMovies() . "\" class=\"modal\">
                <div class=\"modal-content\">
                    <img class='materialboxed' src=\"{$this->getUrl()}\">
                    <div class=\"modaltext\">
                        <h4>{$this->getTitle()}</h4>
                        <p>{$this->getDescriptionMovie()}</p>
                        <div class=\"descfooter\">
                            <h5>{$this->getGender()}</h5>
                            <h5>{$this->getProducer()}</h5>
                            <h5>{$this->getReleaseDate()}</h5>
                        </div>
                        <div class=\"modalpannel\">
                            <form action=\"../include/utils/like.php\" method=\"GET\">
                                <button value=\"" .$this->getIdMovies(). "\" class=\"waves-effect waves-green btn\" name=\"like\" type=\"submit\">Like</button>
                            </form>
                            <h5>" .getlikes($this->getIdMovies()). "</h5>
                            <a href=\"#!\" class=\"modal-close waves-effect waves-green btn\">Agree</a>
                        </div>
                    </div>
                </div>
            </div>";
        
    }
    
    //insert into movies (title, url, descriptionMovie, producer, gender, idUserAuthor) values ('Totoro', 'https://images-na.ssl-images-amazon.com/images/I/91Rd6IjcSWL.jpg', 'Phased bi-directional array', 'Agathe Terrell', 'Documentary', 1);
    
}