<?php


class User
{

    private int $id;
    private string $username;
    private string $email;
    private bool $logged;

    /**
     * User constructor.
     * @param int $id in database
     * @param string $username
     * @param string $email
     * @param bool $logged by session
     */
    public function __construct(int $id, string $username, string $email, bool $logged)
    {
        $this->id = $id;
        $this->username = $username;
        $this->email = $email;
        $this->logged = $logged;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return bool
     */
    public function isLogged(): bool
    {
        return $this->logged;
    }

    /**
     * @param bool $logged
     */
    public function setLogged(bool $logged): void
    {
        $this->logged = $logged;
    }




}