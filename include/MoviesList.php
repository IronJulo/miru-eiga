<?php

require_once "Movie.php";

final class MoviesList
{

    private $movies = [];



    public function addMovie(Movie $movie): void
    {
        $this->$movies[] = $movie;
    }



    public function all(): array
    {
        return $this->$movies;
    }


}