<ul class="dropdown-content dropdowncolor" id="dropdown1">
    <li><a class="white-text" href="../include/utils/logout.php">Log out</a></li>
    <li><a class="white-text" href="home.php">Home</a></li>
    <li><a class="white-text" href="reset-password.php">Reset password</a></li>
    <li class="divider"></li>
    <li><a class="white-text" href="#!">Cancel</a></li>
</ul>

<nav>
    <div class="nav-wrapper">
        <img href="home.php" class="responsive-img" width="65" src="../assets/img/logo.png">
        <a class="brand-logo" href="home.php">Miru Eiga</a>
        <ul class="right hide-on-med-and-down">
            <li>
                <i class="material-icons">account_circle</i>
            </li>
            <!-- Dropdown Trigger -->
            <li><a class="dropdown-trigger" data-target="dropdown1" href="#!">
                    
                    <?php echo htmlspecialchars($_SESSION["username"]); ?>
                    <i class="material-icons right">arrow_drop_down</i>
                </a>
            </li>
        </ul>
    </div>
</nav>

<script>
    $(".dropdown-trigger").dropdown();
</script>