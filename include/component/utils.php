<?php


function isLogged()
{

    return isset($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]) && $_SESSION["loggedin"] === true;
}

function getlikes(int $idMovie)
{
    include "../include/config/config.php";
    $sql = "select count(*) from likes where idMovie = :idmovie;";
    if ($stmt = $pdo->prepare($sql)) {
        $stmt->bindParam(":idmovie", $idMovie, PDO::PARAM_INT);
        if ($stmt->execute()) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            $row = $stmt->fetch();
            return $row["count(*)"];
        }
    }
    return NULL;
}



function getReturnCode(): void
{

    parse_str($_SERVER["QUERY_STRING"], $params);


    $returnType = array_keys($params)[0];

    if (isset($returnType)) {

        $returnCode = $params[$returnType];
        switch ($returnType) {

            case "return" :
                if ($returnCode === "false") {

                    echo "<script>
                                M.toast({html: 'Error, we can\'t add your movie!', classes: 'rounded red-text'});
                             </script>";

                } else {
                    echo "<script>
                             M.toast({html: 'Movie Added', classes: 'rounded green-text'});
                            </script>";
                }
                break;

            case "already" :
                if ($returnCode === "true") {
                    echo "<script>
                                M.toast({html: 'Error, we can\'t add your movie! It already in database!', classes: 'rounded red-text'});
                          </script>";

                }
                break;

            case "remove":
                if ($returnCode === "false") {
                    echo "<script>
                             M.toast({html: 'Error, we can\'t delete your movie!', classes: 'rounded red-text'});
                             </script>";

                } else {
                    echo "<script>
                            M.toast({html: 'Movie Successfully removed!', classes: 'rounded green-text'});
                            </script>";
                }
                break;
            case "update":
                if ($returnCode === "false") {
                    echo "<script>
                               M.toast({html: 'Error, we can\'t update your movie!', classes: 'rounded red-text'});
                               </script>";

                } else {
                    echo "<script>
                               M.toast({html: 'Movie Successfully updated!', classes: 'rounded green-text'});
                                </script>";
                }
                break;

        }


    }
}

function strip_tags_xss( $input ) : string
{
    $input = preg_replace('#</*(?:applet|b(?:ase|gsound|link)|embed|frame(?:set)?|i(?:frame|layer)|l(?:ayer|ink)|meta|object|s(?:cript|tyle)|title|xml)[^>]*+>#i', '', $input);
    $input = preg_replace('#</*\w+:\w[^>]*+>#i', '', $input);

    return $input;

}
function get_all_types(){
    include "../include/config/config.php";
    $res = array();
    $sql = "select distinct gender from movies;";
    if ($stmt = $pdo->prepare($sql)) {
        if ($stmt->execute()) {
            foreach ($stmt->fetchALL() as $row) {
                $res[] = $row["gender"];
            }
        }
    }
    return $res;
}

function get_all_movie_like(string $search)
{
    include "../include/config/config.php";
    $tags = array();
    $tags = explode(" ", $search);
    
    $sql = "SELECT * FROM movies WHERE ";
    $res = array();
    for ($i = 0; $i < count($tags); $i++) {
        if ($i == count($tags) - 1) {
            $sql .= "title like ?";
        } else {
            $sql .= "title like ? or ";
        }
    }
    
    if ($stmt = $pdo->prepare($sql)) {
        $mindex = 1;
        for ($i = 0; $i < count($tags); $i++) {
            $str = "%" . $tags[$i] . "%";
            $stmt->bindParam($mindex++, $str, PDO::PARAM_STR);
        }
        
        if ($stmt->execute()) {
            $stmt->setFetchMode(PDO::FETCH_ASSOC);
            
            while ($row = $stmt->fetch()) {
                $movie = new Movie($row["idMovies"], $row["title"], $row["url"], $row["descriptionMovie"], $row["created_at"], $row["producer"], $row["gender"], $row["release_date"], $row["idUserAuthor"]);
                $res[] = $movie;
            }
        }
    }
    return $res;
}
    
    function get_all_taged_movie(string $search)
    {
        include "../include/config/config.php";
        $search = substr($search, 0, -1);
        $tags = explode(",", $search);
        
        try {
        $sql = "SELECT * FROM movies WHERE ";
        $res = array();
        for ($i = 0; $i < count($tags); $i++) {
            if ($i == count($tags) - 1) {
                $sql .= "gender = :gender".$i;
            } else {
                $sql .= "gender = :gender".$i." or ";
            }
            
        }
        
        if ($stmt = $pdo->prepare($sql)) {
            
            for ($i = 0; $i < count($tags); $i++) {
                $stmt->bindParam(":gender".$i, $tags[$i], PDO::PARAM_STR);
            }
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                while ($row = $stmt->fetch()) {
                    $movie = new Movie($row["idMovies"], $row["title"], $row["url"], $row["descriptionMovie"], $row["created_at"], $row["producer"], $row["gender"], $row["release_date"], $row["idUserAuthor"]);
                    $res[] = $movie;
                }
            }
        }
        }catch (PDOException $e){
            echo $e->getMessage();
        }
        return $res;
    }
   