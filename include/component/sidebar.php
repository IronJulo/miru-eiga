

<div class="row">

<div class="col s4 m4 l2 sidebar" >
    <ul class="collection center-align">
        <form action="searchresult.php" method="GET" name="sres">
            <div>
                <label class="white-text center-align" for="searchbar"><h4>Search</h4></label>
                <input class="white " id="searchbar" name="searchbar" placeholder="Movie or a tag" required type="text">
                <button class="SubmitBtn waves-effect btn" name="submit" type="submit">Search</button>
            </div>
        </form>

        <form action="searchresult.php" method="GET" name="genderres">
            <div class="gendersearchbar">
                    <div class="chips chips-genre input-field">
                        <input autocomplete="off" id="Genre_field">
                    </div>
                    <input type="hidden" id="Genre" name="Genre_Field">
                <button name="submit" type="submit" id="add_btn" class="SubmitBtn waves-effect btn">Search</button>
            </div>
            
        </form>
    </ul>
</div>

