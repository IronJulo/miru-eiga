<script>
    document.addEventListener('DOMContentLoaded', function() {
        document.getElementById("Genre").value = null; //making sure value is nulled when the browser hits back button and gets back to this page.
        var elems = document.querySelectorAll('.chips');
        var chips_genre = M.Chips.init(elems, {
            autocompleteOptions: {
                data: {
                    <?php
                    $types = array();
                    $types = get_all_types();
                    foreach ($types as $type){
                        echo "'".$type."': null,\n                    ";
                    }
                    ?>
                },
                limit: 5,
                minLength: 1 //customise this value according to your requirement
            }
        });
        document.getElementById("add_btn").addEventListener("click", function(event){
            chips_genre.forEach(function(entry) {
                console.log(JSON.stringify(entry.chipsData));
                var dat = entry.chipsData;
                dat.forEach(function (tags){
                    document.getElementById("Genre").value += tags.tag + ","; //using "," as delimiter for multiple tags
                });
            });
        });
    });
</script>