<?php
    
    session_start();
    
    include "../component/islogged.php";
    include "../User.php";
    
    
    $currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);
    
    $likereturn = like($_GET["like"], $currentUser->getId());
    
    function like(int $idMovie, int $idUser)
    {
        //result = 0 : ok like added
        //result = 1 : already liked
        //result = 2 : error
        $result = 0;
        $string = "";
        $sql = "select count(*) from likes where idMovie = :idmovie and idUser = :iduser;";
        include "../config/config.php";
        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":idmovie", $idMovie, PDO::PARAM_INT);
            $stmt->bindParam(":iduser", $idUser, PDO::PARAM_INT);
            if ($stmt->execute()) {
                $stmt->setFetchMode(PDO::FETCH_ASSOC);
                $row = $stmt->fetch();
                if ($row["count(*)"] > 0) {
                    $result = 1;
                } else {
                    $sql = "INSERT INTO likes (idUser, idMovie) values (:iduser, :idmovie);";
                    if ($stmt = $pdo->prepare($sql)) {
                        $stmt->bindParam(":idmovie", $idMovie, PDO::PARAM_INT);
                        $stmt->bindParam(":iduser", $idUser, PDO::PARAM_INT);
                    }
                    if (!$stmt->execute()) {
                        echo $result = 2;
                    }
                }
            } else {
                $result = 2;
            }
        }
        $parts = parse_url($_SERVER['HTTP_REFERER']);
        parse_str(parse_url($_SERVER['HTTP_REFERER'], PHP_URL_QUERY), $query);
        header("location: ../../pages/searchresult.php?searchbar=" .$query["searchbar"]. "&submit=&liked=".$result);
        exit();
        //echo "<script>window.history.back();</script>";
    }