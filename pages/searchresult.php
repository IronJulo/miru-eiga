<?php
    
    session_start();
    
    include "../include/component/islogged.php";
    require_once "../include/User.php";
    require_once "../include/config/config.php";
    
    $currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome Home</title>
    <link rel="stylesheet" href="../assets/css/framework.css">
    <link rel="stylesheet" href="../assets/css/navbar.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="../assets/js/jquery.js"></script>
    <script src="../assets/js/framework.js"></script>
    <link rel="stylesheet" href="../assets/css/searchresult.css">

</head>

<?php
    require_once "../include/User.php";
    require_once "../include/Movie.php";
    
    include "../include/component/navbar.php";
    include "../include/component/sidebar.php";

?>


<div class="col s12 m8 l10 bodycenter">
    <ul class="collection">
        
        <?php
            if (isset($_REQUEST["liked"])){
                switch ($_REQUEST["liked"]){
                    case 0:
                        echo "<script>
                                  M.toast({html: 'You liked this ', classes: 'rounded green-text'});
                              </script>" ;
                        break;
                    case 1:
                        echo "<script>
                                  M.toast({html: 'Error, you already liked this!', classes: 'rounded red-text'});
                              </script>" ;
                        break;
                    case 2:
                        echo "<script>
                                  M.toast({html: 'Error, please try later', classes: 'rounded red-text'});
                              </script>" ;
                        break;
                }
                
            }
            if(isset($_REQUEST['submit'])) {
                    if (isset($_SESSION["id"], $_SESSION["username"]) && isset($_SESSION["email"]) && isset($_SESSION["loggedin"]) && $_SESSION["loggedin"] === true) {
                        if (isset($_REQUEST['searchbar'])) {
                            $movies = get_all_movie_like($_GET["searchbar"]);
                            foreach ($movies as $mov) {
                            $mov->drawCard($currentUser->getId());
                            }
                        }
                        if (isset($_REQUEST['Genre_Field'])) {
                            $movies = get_all_taged_movie($_REQUEST['Genre_Field']);
                            foreach ($movies as $mov) {
                                $mov->drawCard($currentUser->getId());
                            }
                        }
                    } else {
                        echo "<h2>¯\_(ツ)_/¯ How did you got here ¯\_(ツ)_/¯</h2>";
                    }
                }
        ?>

</div>

<script>
    $(document).ready(function () {
        $('.modal').modal();
    });

    $(document).ready(function () {
        $('.materialboxed').materialbox();
    });
</script>
<?php
    include "../include/component/chipsscript.php";
?>
</html>
