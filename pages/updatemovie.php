<?php


session_start();

include "../include/component/islogged.php";
require_once "../include/User.php";
require_once "../include/config/config.php";
require_once "../include/component/utils.php";


if (isLogged()) {

    $currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);

    if (isset($_GET["id"], $_GET["title"], $_GET["description"], $_GET["url"], $_GET["gender"], $_GET["producer"], $_GET["release"])) {

        $id = trim($_GET["id"]);
        $title = trim($_GET["title"]);
        $description = trim($_GET["description"]);
        $url = trim($_GET["url"]);
        $gender = trim($_GET["gender"]);
        $producer = trim($_GET["producer"]);
        $release = trim($_GET["release"]);
        $userId = $currentUser->getId();

        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            header("location: home.php?update=false");
            exit;
        }

        $data = [
            'title' => $title,
            "url" => $url,
            "descriptionMovie" => $description,
            "gender" =>strtolower($gender),
            "producer" => $producer,
            "release_date" => strtolower($release),
            'id' => $id
        ];

        $sql = "UPDATE movies SET title=:title, url=:url, descriptionMovie=:descriptionMovie, producer=:producer,gender=:gender ,release_date=:release_date WHERE idMovies = :id";


        if ($stmt = $pdo->prepare($sql)){

            $returnCode = true;

            if(!$stmt->execute($data)){
                header("location: home.php?update=false");
                exit;

            } else {
                header("location: home.php?update=true");
                exit;
            }
        }

    }
}
