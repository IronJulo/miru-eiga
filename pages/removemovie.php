<?php

session_start();

include "../include/component/islogged.php";
require_once "../include/User.php";
require_once "../include/component/utils.php";

require_once "../include/config/config.php";

if (isLogged()) {


    $currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);

    if (isset($_GET["id"])) {

        $filmToRemove = (int)trim($_GET["id"]);


        $deleteInMovie = delete_by_id_in('movies', 'idMovies', $filmToRemove, $pdo);
        $deleteInLikes =delete_by_id_in('likes', 'idMovie', $filmToRemove, $pdo);

        if ($deleteInMovie && $deleteInLikes ) {
            header("location: home.php?remove=true");
            exit;
        } else {
            header("location: home.php?remove=false");
            exit;
        }
    }

}


function delete_by_id_in(string $tableName, string $col, int $id, PDO $pdo)
{


    $allowed_tables_array = array($tableName);
    $allowed_columns_array[$tableName] = array($col);

    if (in_array($tableName, $allowed_tables_array) && in_array($col, $allowed_columns_array[$tableName])) {


        $sql = "DELETE FROM $tableName WHERE $col = :id";

        if ($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(":id", $id, PDO::PARAM_INT);

            $stmt->execute();

            return !($stmt->rowCount() === 0);
        }
    }

}
