<?php

session_start();

include "../include/component/islogged.php";
require_once "../include/User.php";
require_once "../include/config/config.php";

$currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Welcome Home</title>
    <link rel="stylesheet" href="../assets/css/framework.css">
    <link rel="stylesheet" href="../assets/css/navbar.css">
    <link rel="stylesheet" href="../assets/css/home.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script type="text/javascript" src="../assets/js/jquery.js"></script>
    <script src="../assets/js/framework.js"></script>
</head>

<?php


/* Class */
require_once "../include/User.php";  // manage User
require_once "../include/Movie.php"; // movie type

/* Components */
include "../include/component/navbar.php";
include "../include/component/sidebar.php";

/* Utils */

require_once "../include/component/utils.php"; // provide isLogged() getReturnCode()

getReturnCode();

?>


<div class="col s12 m8 l10 bodycenter">
    <!-- button new-->
    <div class="fixed-action-btn">
        <a class="btn-floating btn-large purple darken-2">
            <i class="large material-icons">mode_edit</i>
        </a>
        <ul>
            <!-- add modal trigger-->
            <li><a href="#addmodal" class="btn-floating purple modal-trigger darken-2"><i class="material-icons">add</i></a>
            </li>
        </ul>
    </div>
    <script>
        $(document).ready(function () {
            $('.fixed-action-btn').floatingActionButton();
        });
        document.addEventListener('DOMContentLoaded', function () {
            var elems = document.querySelectorAll('.fixed-action-btn');
            var instances = M.FloatingActionButton.init(elems, {
                direction: 'left',
                hoverEnabled: false
            });
        });
    </script>


    <!-- Modal Add new movie -->
    <div id="addmodal" class="modal modal-fixed-footer">
        <div class="modal-content">
            <h4>Add new movie</h4>

            <div class="row">
                <form class="col s12" id="addmovie" method="GET" action="./addmovie.php">

                    <div class="row">
                        <div class="input-field col s6">
                            <input name="title"  maxlength="150" id="title" type="text" class="validate">
                            <label for="title">Title</label>
                        </div>


                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <textarea name="desc" id='desc' maxlength="1000" class="materialize-textarea"></textarea>
                            <label for="desc">Description</label>
                        </div>
                    </div>


                    <div class="row">
                        <div class="input-field col s12 l6">
                            <textarea name="url" id='urlPicture' class="materialize-textarea"></textarea>
                            <label for="urlPicture">URL Movie Picture</label>
                        </div>
                        <div class="input-field col s6 l6">
                            <img class="materialboxed" id="picture" width="100em" src="">
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12 l6">
                            <textarea id='gender' maxlength="50" name="gender" class="materialize-textarea"></textarea>
                            <label for="gender">Gender</label>
                        </div>
                        <div class="input-field col s12 l6">
                            <textarea id='prod' maxlength="50" name="prod" class="materialize-textarea"></textarea>
                            <label for="prod">Producer</label>
                        </div>
                    </div>

                    <div class='row'>
                        <div class="input-field col s12">
                            <textarea class="materialize-textarea" name="release" maxlength="50" id="release"></textarea>
                            <label for="release">Date of release</label>
                        </div>
                    </div>


                </form>
            </div>

        </div>
        <div class="modal-footer">
            <a href="#!" class="modal-close waves-effect waves-green btn-flat red-text">Cancel</a>
            <a href="#!" id="agreeBtn" class="modal-close waves-effect waves-green btn-flat green-text">Agree</a>
        </div>
    </div>


    <ul class="collection ">

        <?php

        if (isLogged()) {

            //echo "<h4>Hello , {$currentUser->getUsername()}</h4>";

            $sql = "SELECT * FROM movies WHERE idUserAuthor = :currentUserId";


            if ($stmt = $pdo->prepare($sql)) {
                $id = $currentUser->getId();
                $stmt->bindParam(":currentUserId", $id, PDO::PARAM_INT);
                $param_id = trim($id);

                if ($stmt->execute()) {
                    foreach ($stmt->fetchALL() as $row) {
                        $movie = new Movie($row[0], $row[1], $row[2], $row[3], $row[4], $row[5], $row[6], $row[7], $row[8]);
                        $movie->drawListItem();


                    }
                }

            } else {
                echo "<h2>You dont have any movie presentation maybe try to create one (づ｡◕‿‿◕｡)づ </h2>";
            }
        }

        ?>

</div>
<script>
    if(typeof window.history.pushState == 'function') {
        var host = "sabisu.me/.website/miru-eiga"; // replace here with you server path
        window.history.pushState({}, "Hide", "http://" + host + "/pages/home.php");
    }
</script>
<script>
    $(document).ready(function () {
        $('.modal').modal();
    });

    $(document).ready(function () {
        $('.materialboxed').materialbox();
    });

    $('document').ready(function () {
        $("#urlPicture").change(function () {
            var text = $('textarea#urlPicture').val();
            $('#picture').attr('src', text);

        });
    });

    $("#agreeBtn").click(function() {
        $("#addmovie").submit();
    });
</script>
<?php
    include "../include/component/chipsscript.php";
?>
</html>