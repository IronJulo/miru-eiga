<?php

session_start();

include "../include/component/islogged.php";
require_once "../include/User.php";
require_once "../include/config/config.php";
require_once "../include/component/utils.php";


if(isLogged()) {

    $currentUser = new User($_SESSION["id"], $_SESSION["username"], $_SESSION["email"], $_SESSION["loggedin"]);

    if (isset($_GET["title"], $_GET["desc"], $_GET["url"],$_GET["gender"],$_GET["prod"],$_GET["release"] )) {

        $title = trim($_GET["title"]);
        $description = trim($_GET["desc"]);
        $url = trim($_GET["url"]);
        $gender = trim($_GET["gender"]);
        $producer = trim($_GET["prod"]);
        $release = trim($_GET["release"]);
        $userId = $currentUser->getId();

        if (filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
            header("location: home.php?return=false");
            exit;
        }

        $data = [
            'title' => $title,
            "url" => $url,
            "description" => $description,
            "gender" =>strtolower($gender),
            "producer" => $producer,
            "release_date" => strtolower($release),
            "userId" =>strtolower($userId)

        ];


        $sql = "INSERT INTO movies (title, url, descriptionMovie , producer, gender , release_date , idUserAuthor) VALUES (:title, :url, :description, :producer,:gender ,:release_date,:userId)";


        if ($stmt = $pdo->prepare($sql)){

            $returnCode = true;
            // TODO try catch error on violation
            try {
                if(!$stmt->execute($data)){
                    header("location: home.php?return=false");
                    exit;

                } else {
                    header("location: home.php?return=true");
                    exit;
                }
            } catch (PDOException $e) {
                if ($e->getCode() === '23000') {
                    // echo "Syntax Error: " . $e->getMessage();
                    header("location: home.php?already=true");
                    exit;
                }
            }

        }
        header("location: home.php?return=false");
        exit;



    } else {
        header("location: home.php?return=false");
        exit;
    }

} else {

    header("location: login.php");
    exit;
}

?>
