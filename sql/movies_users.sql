create table users
(
    id         int auto_increment
        primary key,
    username   varchar(50)                          not null,
    email      varchar(255)                         not null,
    password   varchar(255)                         not null,
    created_at datetime default current_timestamp() null,
    constraint email
        unique (email),
    constraint username
        unique (username)
);

INSERT INTO movies.users (id, username, email, password, created_at) VALUES (1, 'user1', 'user1@user1.user1', '$2y$10$3iq2kpLthmlwAT/HuO2ZOenKyxXjTWGqw99REC9zsrjIcAQ6YHWFS', '2020-10-13 18:20:11');
INSERT INTO movies.users (id, username, email, password, created_at) VALUES (2, 'user2', 'user2.user2@user2', '$2y$10$ODWfnvxY4eRL8G0i6nVGG.oyj6IHt0jWRE70amh.GCJQpAlI4kKM.', '2020-10-13 18:20:38');
INSERT INTO movies.users (id, username, email, password, created_at) VALUES (3, 'dayan', 'dayan@dayan.dayan', '$2y$10$ohh0rMdDv9tG9KDvKMoMtOCsDijNnUEjculqTd/YGqaNwilwPUXGm', '2020-10-17 00:45:12');
INSERT INTO movies.users (id, username, email, password, created_at) VALUES (4, 'jojo', 'jojo@jojo.jojo', '$2y$10$ioAozksUkYreTG1sqg3Ske1p.Icj3Km54JrKg0hKtY78UaRR5Ubmi', '2020-10-17 05:29:27');
INSERT INTO movies.users (id, username, email, password, created_at) VALUES (5, 'user5', 'user5@sabisu.me', '$2y$10$hWJYjSRmvqXRW8MdINr1kO73wrEjL0J1phBdLuIe3lO6tEstyaLxS', '2020-10-17 10:41:15');
INSERT INTO movies.users (id, username, email, password, created_at) VALUES (6, 'user4', 'user4@user4.com', '$2y$10$WiaMNdN7jDTRdtcYbvSqdOstVJXi4NubwfNpLQcnm/SRcHoyZpRIW', '2020-10-18 11:40:21');