create table likes
(
    idUser  int not null,
    idMovie int not null
);

INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 1);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 2);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 3);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 4);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 5);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 6);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 7);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 9);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 10);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 12);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 13);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 14);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 15);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 16);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 17);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 18);
INSERT INTO movies.likes (idUser, idMovie) VALUES (5, 19);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 3);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 4);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 5);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 9);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 10);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 3);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 24);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 7);
INSERT INTO movies.likes (idUser, idMovie) VALUES (7, 7);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 7);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 9);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 4);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 10);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 12);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 13);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 5);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 16);
INSERT INTO movies.likes (idUser, idMovie) VALUES (4, 17);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 23);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 6);
INSERT INTO movies.likes (idUser, idMovie) VALUES (1, 22);